package inf.javaee.demo;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.springframework.data.mongodb.core.mapping.Document;
import javax.persistence.*;
import java.util.UUID;

@Data
@Document
@NoArgsConstructor
public class Person {
    @Id
    private String Id = UUID.randomUUID().toString();
    private String nick;
    private String pass;
    private String email;
    boolean admin = false;

}
