package inf.javaee.demo.services;

import inf.javaee.demo.Person;
import org.springframework.data.mongodb.repository.Query;
import org.springframework.data.mongodb.repository.ReactiveMongoRepository;
import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;

interface PersonRepository extends ReactiveMongoRepository<Person, String> {
    @Query("{ 'nick': ?0 }")
    Flux<Person> findByNick(final String nick);

    Mono<Void> deleteById(int id);

    Mono<Person> findById(int id);
}