package inf.javaee.demo.services;



import inf.javaee.demo.Person;


import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;

@RestController
class PersonServiceMethods implements MethodsPersonServ {
    @Autowired
    private PersonRepository prepo;


    @Override
    public void create(Person p) {
        prepo.save(p).subscribe();
    }

    @Override
    public void deleteEverything(int Id) {
        prepo.deleteAll();
    }

    @Override
    public Mono<Void> delete(int Id) {
        return prepo.deleteById(Id);
    }

    @Override
    public Flux<Person> findThemAll() {
        return prepo.findAll();
    }

    @Override
    public Mono<Person> findThemById(int Id) {
        return prepo.findById(Id);
    }

    @Override
    public Flux<Person> findThemByNick(String nick) {
        return prepo.findByNick(nick);
    }

    @Override
    public Mono<Person> update(Person p) {
        return prepo.save(p);
    }
}








