package inf.javaee.demo.services;


import inf.javaee.demo.Person;
import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;

public interface MethodsPersonServ {
    void create(Person p);
    void deleteEverything(int Id);
    Mono<Void> delete(int id);
    Flux<Person> findThemAll();
    Mono<Person> findThemById(int id);
    Flux<Person> findThemByNick(String nick);
    Mono<Person>update(Person p);

}
