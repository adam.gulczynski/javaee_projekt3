package inf.javaee.demo.Controlls;
import inf.javaee.demo.Person;
import inf.javaee.demo.services.MethodsPersonServ;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;


@RestController
public class Controllers {

    MethodsPersonServ mps;

    @RequestMapping(value = "/create", method = RequestMethod.POST)
    @ResponseStatus(HttpStatus.CREATED)
    public void create(@RequestBody Person person) {
        mps.create(person);
    }

    @GetMapping("/get")
    public Flux<Person> findAll() {
        return mps.findThemAll();
    }

    @RequestMapping(value = "person/{id}", method = RequestMethod.GET)
    public ResponseEntity<Mono<Person>> findThemById(@PathVariable("id") Integer id) {
        Mono<Person> person = mps.findThemById(id);
        HttpStatus status = person != null ? HttpStatus.OK : HttpStatus.NOT_FOUND;
        return new ResponseEntity<>(person, status);
    }

    @RequestMapping(value = "person/delete/{id}", method = RequestMethod.DELETE)
    @ResponseStatus(HttpStatus.OK)
    public void delete(@PathVariable("id") Integer id) {
        mps.delete(id).subscribe();
    }



    @PutMapping("/update")
    public Mono<Person> update(@RequestBody Person p) {
        return mps.update(p);
    }
}